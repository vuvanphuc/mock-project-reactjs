export interface ILoginResponse {

}
export interface IInforUser {
    ten_tai_khoan: string;
    mat_khau: string;
}

export interface IInforUserRegister {
    ten_tai_khoan?: string;
    ten_nhan_vien?: string;
    email?: string;
    mat_khau?: string;
    xac_nhan_mat_khau?: string;
    id?: string;
}

export interface IContentNotification {
    message: string
    description: string
}

export interface IDataTablePosts {
    key: string;
    anh_dai_dien: string;
    mo_ta: string;
    ngay_sua: string;
    ngay_tao: string;
    nguoi_sua: string;
    nguoi_tao: string;
    nhom_tin_tuc_id: number;
    noi_dung: string;
    tieu_de: string;
    tin_moi: number;
    tin_noi_bat: number;
    tin_tuc_id: number;
    trang_thai: string;
}

export interface IDataTableUsers {
    key: string;
    anh_dai_dien: string;
    chuc_vu: string;
    dia_chi: string;
    don_vi: string;
    email: string;
    gioi_thieu: string;
    gioi_tinh: number;
    ngay_sinh: string;
    ngay_sua: string | null;
    ngay_tao: string;
    nguoi_sua: string | null;
    nguoi_tao: string;
    nhan_vien_id: string;
    nhom_nhan_vien: IUserGroup;
    nhom_nhan_vien_id: string;
    so_dien_thoai: string;
    ten_nhan_vien: string;
    ten_tai_khoan: string;
    trang_thai: string;
}

export interface IUserGroup {
    ten_nhom: string;
    nhom_nhan_vien_id: string;
    ma_nhom: string;
    mo_ta: string;
    trang_thai: string;
}

export interface IBodyUpdatePost {
    tieu_de: string;
    mo_ta: string;
    noi_dung: string;
    nhom_tin_tuc_id: string;
    tin_noi_bat: number;
    tin_moi: number;
    trang_thai: string;
    id?: string;
}

export interface IBodyAddPost {
    tieu_de: string;
    nguoi_nhan: string;
    mo_ta: string;
    noi_dung: string;
    nhom_tin_tuc_id: string;
    tin_noi_bat: number;
    tin_moi: number;
    trang_thai: string;
  }




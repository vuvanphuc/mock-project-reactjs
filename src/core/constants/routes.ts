const routes = {
    login: '/',
    register: '/register',
    forgot: '/forgot',
    dashboard: '/dashboard',
    profile: '/profile',
    adminPosts: '/admin/posts',
    adminUsers: '/admin/users',
    setting: '/admin/setting'
}

export default routes;
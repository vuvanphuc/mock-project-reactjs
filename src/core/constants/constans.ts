export const URL_BASE = 'https://exam-dev-api.web5days.com:5001/';

export const auth = {
    USER_INFO: 'USER_INFO',
    TOKEN: 'TOKEN'
}

export const statusNotification = {
    login: {
        LOGIN_SUCCESS: 'LOGIN_SUCCESS',
        LOGIN_FAIL: 'LOGIN_FAIL'
    },
    register: {
        REGISTER_SUCCESS: 'REGISTER_SUCCESS',
        REGISTER_FAIL: 'REGISTER_FAIL'
    }
}
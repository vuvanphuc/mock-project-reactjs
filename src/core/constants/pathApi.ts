export const pathApi = {
    auth: {
        login: 'user/login',
        regiter: 'user/regUser'
    },
    post: {
        posts: 'posts',
        deletePost: 'post',
        updatePost: 'post',
        addPost: 'post'
    },
    user: {
        users: 'users',
        deleteUser: 'user',
        updateUser: 'user',
        addUser: 'user/regUser'
    },
    category: {
        categories: 'post-categories',
        postsByCategory: 'post-category',
    }
}
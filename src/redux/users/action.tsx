import { createAsyncThunk } from "@reduxjs/toolkit";
import { pathApi } from "../../core/constants/pathApi";
import { IInforUserRegister } from "../../core/models/models";
import httpClient from "../api";
import { ADD_USER, DELETE_USER, GET_LIST_USER, UPDATE_USER } from "./type";

export const fetchUser = createAsyncThunk(GET_LIST_USER, async (thunkAPI) => {
  const response = await httpClient.get(pathApi.user.users);
  return response.data;
});

export const deleteUser = createAsyncThunk(DELETE_USER, async (id: string, thunkAPI) => {
  const response = await httpClient.delete(`${pathApi.user.deleteUser}/${id}`);
  if (response.data.success) {
    return id;
  } else {
    return null;
  }
});

export const addUser = createAsyncThunk(ADD_USER, async (body: IInforUserRegister, thunkAPI) => {
  const response = await httpClient.post(pathApi.user.addUser, body);
  if (response.data.success && response.data.data) {
    return response.data.data;
  } else {
    return null;
  }
});

export const updateUser = createAsyncThunk(UPDATE_USER, async (data: IInforUserRegister, thunkAPI) => {
  const body = {...data};
  delete body.id;
  const response = await httpClient.put(`${pathApi.user.updateUser}/${data.id}`, body);
  if (response.data.success && response.data.data) {
    return {
      data: response.data.data,
      id: data.id
    };
  } else {
    return null;
  }
});

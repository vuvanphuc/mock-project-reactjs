import { createSlice } from "@reduxjs/toolkit";
import { fetchUser, deleteUser, addUser, updateUser } from "./action";
import { notification } from 'antd';

const initialState: any = {
  list: {
    loading: false,
    result: {},
    error: {},
  },
}

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // get list user
      .addCase(fetchUser.pending, (state) => {
        state.list.loading = true;
        state.list.result = {};
        state.list.error = {};
      })
      .addCase(fetchUser.fulfilled, (state, action) => {
        state.list.loading = false;
        state.list.result = action.payload;
        state.list.error = {};
      })
      .addCase(fetchUser.rejected, (state, action) => {
        state.list.loading = false;
        state.list.result = {};
        state.list.error = action.error;
      })

      // delete user
      .addCase(deleteUser.pending, (state) => {
        state.list.loading = true;
        state.list.error = {};
      })
      .addCase(deleteUser.fulfilled, (state, action) => {
        state.list.loading = false;
        if (action.payload) {
          state.list.result.data = [...state.list.result.data.filter((item: any) => item.nhan_vien_id !== action.payload)]
          notification.success({
            message: 'Xóa người dùng thành công',
            description: 'Bạn đã xóa người dùng thành công!'
          })
        } else {
          notification.error({
            message: 'Xóa người dùng thất bại',
            description: 'Bạn đã xóa người dùng thất bại!'
          })
        }
        state.list.error = {};
      })
      .addCase(deleteUser.rejected, (state, action) => {
        state.list.loading = false;
        state.list.error = action.error;
        notification.error({
          message: 'Xóa người dùng thất bại',
          description: 'Bạn đã xóa người dùng thất bại!'
        })
      })
      
      // add user
      .addCase(addUser.pending, (state) => {
        state.list.loading = true;
        state.list.error = {};
      })
      .addCase(addUser.fulfilled, (state, action) => {
        state.list.loading = false;
        if (action.payload) {
          state.list.result.data.unshift(action.payload);
          notification.success({
            message: 'Thêm người dùng thành công',
            description: 'Bạn đã thêm người dùng thành công!'
          })
        } else {
          notification.error({
            message: 'Thêm người dùng thất bại',
            description: 'Bạn đã thêm người dùng thất bại!'
          })
        }
        state.list.error = {};
      })
      .addCase(addUser.rejected, (state, action) => {
        state.list.loading = false;
        state.list.error = action.error;
        notification.error({
          message: 'Thêm người dùng thất bại',
          description: 'Bạn đã thêm người dùng thất bại!'
        })
      })

      // update user
      .addCase(updateUser.pending, (state) => {
        state.list.loading = true;
        state.list.error = {};
      })
      .addCase(updateUser.fulfilled, (state, action) => {
        state.list.loading = false;
        if (action.payload) {
          state.list.result.data.forEach((item: any, index: number) => {
            if (item.nhan_vien_id === action?.payload?.id) {
              state.list.result.data.splice(index, 1, action?.payload?.data);
            }
          })
          notification.success({
            message: 'Sửa người dùng thành công',
            description: 'Bạn đã sửa người dùng thành công!'
          })
        } else {
          notification.error({
            message: 'Sửa người dùng thất bại',
            description: 'Bạn đã sửa người dùng thất bại!'
          })
        }
        state.list.error = {};
      })
      .addCase(updateUser.rejected, (state, action) => {
        state.list.loading = false;
        state.list.error = action.error;
        notification.error({
          message: 'Sửa người dùng thất bại',
          description: 'Bạn đã sửa người dùng thất bại!'
        })
      });
  },
});

export default userSlice.reducer;


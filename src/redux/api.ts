import axios from 'axios';
import { notification } from 'antd';
import { getLocal } from '../core/utils/localStorage';
import { auth } from '../core/constants/constans';

const httpClient = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    headers: {
        'Content-Type': 'application/json',
    },
});

httpClient.interceptors.request.use(
    (config) => {
        const token = getLocal(auth.TOKEN);
        if (token && config.headers) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        Promise.reject(error);
    }
)

httpClient.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error.response) {
            if (error.response.status === 401) {
                notification.error({
                    message: 'Lỗi đăng nhập tài khoản',
                    description: 'Bạn vui lòng đăng nhập lại!'
                });
                setTimeout(() => {
                    localStorage.clear();
                }, 5000);
            }
            if (error.response.status === 500) {
                notification.error({
                    message: 'Hệ thống đã xảy ra lỗi',
                    description: 'Vui lòng kết nối lại!'
                });
            }
        }
        return Promise.reject(error);
    }
);

const getHeader = (): any => {
    const token = localStorage.getItem('userToken');
    return {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
    };
};

const getHeaderUpload = (): any => {
    const token = localStorage.getItem('userToken');
    return {
        Authorization: `Bearer ${token}`,
    };
};

export function getApi(url: string) {
    return axios({
        method: 'get',
        url,
    });
}

export function deleteApi(url: string, data: any): any {
    return axios({
        method: 'delete',
        url,
        data: { id: data.id },
        headers: getHeader(),
    });
}

export function postApi(url: string, data: any): any {
    return axios({
        method: 'post',
        url,
        data,
        headers: getHeader(),
    });
}

export function putApi(url: string, data: any): any {
    return axios({
        method: 'put',
        url,
        data,
        headers: getHeader(),
    });
}

export function uploadApi(url: string, data: any): any {
    let formData = new FormData();
    formData.append('fileUpload', data.fileUpload);
    formData.append('folder', data.folder);
    return axios({
        method: 'post',
        url,
        data: formData,
        headers: getHeaderUpload(),
    });
}

export default httpClient;

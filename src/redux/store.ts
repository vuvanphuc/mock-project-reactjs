import { configureStore } from '@reduxjs/toolkit';
import userReducer from './users/reducer';
import postReducer from './posts/reducer';

const reducer = {
  users: userReducer,
  posts: postReducer,
};

const store = configureStore({
  reducer: reducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

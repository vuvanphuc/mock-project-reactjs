import { createAsyncThunk } from "@reduxjs/toolkit";
import { pathApi } from "../../core/constants/pathApi";
import { IBodyAddPost, IBodyUpdatePost } from "../../core/models/models";
import httpClient from "../api";
import { ADD_POST, DELETE_POST, GET_LIST_POST, UPDATE_POST } from "./type";

export const fetchPosts = createAsyncThunk(GET_LIST_POST, async (thunkAPI) => {
  const response = await httpClient.get(pathApi.post.posts);
  return response.data;
});

export const deletePost = createAsyncThunk(DELETE_POST, async (id: string, thunkAPI) => {
  const response = await httpClient.delete(`${pathApi.post.deletePost}/${id}`);
  if (response.data.success) {
    return id;
  } else {
    return null;
  }
});

export const addPost = createAsyncThunk(ADD_POST, async (body: IBodyAddPost, thunkAPI) => {
  const response = await httpClient.post(pathApi.post.addPost, body);
  if (response.data.success && response.data.data) {
    return response.data.data;
  } else {
    return null;
  }
});

export const updatePost = createAsyncThunk(UPDATE_POST, async (data: IBodyUpdatePost, thunkAPI) => {
  const body = {...data};
  delete body.id;
  const response = await httpClient.put(`${pathApi.post.updatePost}/${data.id}`, body);
  if (response.data.success && response.data.data) {
    return {
      data: response.data.data,
      id: data.id
    };
  } else {
    return null;
  }
});

import { createSlice } from "@reduxjs/toolkit";
import { fetchPosts, deletePost, updatePost, addPost } from "./action";
import { notification } from 'antd';

const initialState: any = {
  list: {
    loading: false,
    result: {},
    error: {},
  },
}

export const postSlice = createSlice({
  name: "post",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      // get list post
      .addCase(fetchPosts.pending, (state) => {
        state.list.loading = true;
        state.list.result = {};
        state.list.error = {};
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.list.loading = false;
        state.list.result = action.payload;
        state.list.error = {};
      })
      .addCase(fetchPosts.rejected, (state, action) => {
        state.list.loading = false;
        state.list.result = {};
        state.list.error = action.error;
      })

      // delete post
      .addCase(deletePost.pending, (state) => {
        state.list.loading = true;
        state.list.error = {};
      })
      .addCase(deletePost.fulfilled, (state, action) => {
        state.list.loading = false;
        if (action.payload) {
          state.list.result.data = [...state.list.result.data.filter((item: any) => item.tin_tuc_id !== Number(action.payload))];
          notification.success({
            message: 'Xóa bài viết thành công',
            description: 'Bạn đã xóa bài viết thành công!'
          })
        } else {
          notification.error({
            message: 'Xóa bài viết thất bại',
            description: 'Bạn đã xóa bài viết thất bại!'
          })
        }
        state.list.error = {};
      })
      .addCase(deletePost.rejected, (state, action) => {
        state.list.loading = false;
        state.list.error = action.error;
        notification.error({
          message: 'Xóa bài viết thất bại',
          description: 'Bạn đã xóa bài viết thất bại!'
        })
      })

      // add post
      .addCase(addPost.pending, (state) => {
        state.list.loading = true;
        state.list.error = {};
      })
      .addCase(addPost.fulfilled, (state, action) => {
        state.list.loading = false;
        if (action.payload) {
          state.list.result.data.unshift(action.payload);
          notification.success({
            message: 'Thêm bài viết thành công',
            description: 'Bạn đã thêm bài viết thành công!'
          })
        } else {
          notification.error({
            message: 'Thêm bài viết thất bại',
            description: 'Bạn đã thêm bài viết thất bại!'
          })
        }
        state.list.error = {};
      })
      .addCase(addPost.rejected, (state, action) => {
        state.list.loading = false;
        state.list.error = action.error;
        notification.error({
          message: 'Thêm bài viết thất bại',
          description: 'Bạn đã thêm bài viết thất bại!'
        })
      })

      // update post
      .addCase(updatePost.pending, (state) => {
        state.list.loading = true;
        state.list.error = {};
      })
      .addCase(updatePost.fulfilled, (state, action) => {
        state.list.loading = false;
        if (action.payload) {
          state.list.result.data.forEach((item: any, index: number) => {
            if (item.tin_tuc_id === Number(action?.payload?.id)) {
              state.list.result.data.splice(index, 1, action?.payload?.data);
            }
          })
          notification.success({
            message: 'Sửa bài viết thành công',
            description: 'Bạn đã sửa bài viết thành công!'
          })
        } else {
          notification.error({
            message: 'Sửa bài viết thất bại',
            description: 'Bạn đã sửa bài viết thất bại!'
          })
        }
        state.list.error = {};
      })
      .addCase(updatePost.rejected, (state, action) => {
        state.list.loading = false;
        state.list.error = action.error;
        notification.error({
          message: 'Sửa bài viết thất bại',
          description: 'Bạn đã sửa bài viết thất bại!'
        })
      });
  },
});

export default postSlice.reducer;


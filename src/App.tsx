import React, { Suspense } from "react";
import "./assets/styles/style.scss";
import "antd/dist/antd.min.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { getLocal } from "./core/utils/localStorage";
import { auth } from "./core/constants/constans";
import Guard from "./components/guard/Guard";
import GuardLogin from "./components/guard-login/GuardLogin";
import GuardAdmin from "./components/guard-admin/GuardAdmin";
import GuardUser from "./components/guard-user/GuardUser";
import Loading from "./components/loading/Loading";
import store from './redux/store';
import { Provider } from 'react-redux';

const Layout = React.lazy(() => import('./components/layout/Layout'));
const NotFound = React.lazy(() => import('./components/not-found/NotFound'));
const DashBoard = React.lazy(() => import('./pages/dashboard/DashBoard'));

const Login = React.lazy(() => import('./pages/auth/components/login/Login'));
const Register = React.lazy(() => import('./pages/auth/components/register/Register'));
const Forgot = React.lazy(() => import('./pages/auth/components/forgot/Forgot'));

const Posts = React.lazy(() => import('./pages/dashboard/compoments/admin/posts/posts/Posts'));
const PostDetail = React.lazy(() => import('./pages/dashboard/compoments/admin/posts/postDetail/PostDetail'));

const Users = React.lazy(() => import('./pages/dashboard/compoments/admin/users/Users'));

const Home = React.lazy(() => import('./pages/dashboard/compoments/publish/home/Home'));
const Profile = React.lazy(() => import('./pages/dashboard/compoments/publish/profile/Profile'));
const Setting = React.lazy(() => import('./pages/dashboard/compoments/publish/setting/Setting'));

function App() {
  /**
   * listen token in localstorage
   */
  window.addEventListener(
    'storage',
    () => {
      if (!getLocal(auth.TOKEN)) {
        window.location.reload();
      }
    },
    true
  );

  return (
    <Provider store={store}>
      <Suspense fallback={<Loading />}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Layout />}>

              <Route element={<GuardLogin />}>
                <Route index element={<Login />} />
                <Route path="register" element={<Register />} />
                <Route path="forgot" element={<Forgot />} />
              </Route>
              
              <Route element={<Guard />}>
                <Route path="dashboard" element={<DashBoard />}>
                  <Route element={<GuardUser />}>
                    <Route index element={<Home />} />
                  </Route>

                  <Route element={<GuardAdmin />}>
                    <Route path="admin/posts" element={<Posts />}>
                      <Route path=":id" element={<PostDetail />} />
                    </Route>
                    <Route path="admin/users" element={<Users />} />
                    <Route path="admin/setting" element={<Setting />} />
                  </Route>
                  
                  <Route path="profile" element={<Profile />} />
                  <Route path='*' element={<NotFound />} />
                </Route>
              </Route>

            </Route>
          </Routes>
        </BrowserRouter>
      </Suspense>
    </Provider>
  );
}

export default App;

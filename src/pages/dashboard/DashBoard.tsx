import { useState } from "react";
import styles from "./DashBoard.module.scss";
import {
  HomeOutlined,
  SettingOutlined,
  DownOutlined,
  UserOutlined,
  LogoutOutlined,
  UsergroupAddOutlined,
  BookOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Dropdown, Space, Modal } from "antd";
import { Link, Outlet, useNavigate } from "react-router-dom";
import routes from "../../core/constants/routes";
import { clearLocal, getObjectLocal } from "../../core/utils/localStorage";
import { auth, URL_BASE } from "../../core/constants/constans";

import logo from "../../assets/images/logo-login.png";
import avatarDefault from "../../assets/images/avatar.png";

const { Header, Content, Footer, Sider } = Layout;

function DashBoard() {
  const [userInfo, setUserInfo] = useState<any>(() => {
    const userInfoLocal = localStorage.getItem(auth.USER_INFO);
    if (userInfoLocal) {
      return getObjectLocal(auth.USER_INFO);
    } else {
      return {};
    }
  });
  const [collapsed, setCollapsed] = useState<boolean>(false);
  const [isModalLogout, setIsModalLogout] = useState<boolean>(false);
  const navigate = useNavigate();

  const showModalLogout = (): void => {
    setIsModalLogout(true);
  };

  const handleOkLogout = (): void => {
    handleLogout();
  };

  const handleCancelLogout = (): void => {
    setIsModalLogout(false);
  };

  const handleLogout = (): void => {
    setIsModalLogout(false);
    navigate(routes.login);
    clearLocal();
  };

  const menu = (
    <Menu
      items={[
        {
          label: (
            <Link to={`${routes.dashboard}${routes.profile}`}>
              Trang cá nhân
            </Link>
          ),
          key: "0",
          icon: <UserOutlined />,
        },
        {
          type: "divider",
        },
        {
          label: (
            <p className={styles.btn_logout} onClick={showModalLogout}>
              Đăng xuất
            </p>
          ),
          key: "1",
          icon: <LogoutOutlined />,
        },
      ]}
    />
  );

  return (
    <>
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
          theme="light"
        >
          <div className={styles.wrapper_logo}>
            <Link
              className={styles.link_logo}
              to={
                userInfo?.ma_nhom_nhan_vien === "admin"
                  ? `${routes.dashboard}${routes.adminPosts}`
                  : `${routes.dashboard}`
              }
            >
              <img className={styles.logo} src={logo} alt="logo" />
            </Link>
          </div>

          <Menu theme="light" defaultSelectedKeys={userInfo?.ma_nhom_nhan_vien !== "admin" ? ["1"] : ["2"]} mode="inline">
            {userInfo?.ma_nhom_nhan_vien !== "admin" && (
              <Menu.Item icon={<HomeOutlined />} key={"1"}>
                <Link defaultChecked={true} to={`${routes.dashboard}`}>
                  Trang chủ
                </Link>
              </Menu.Item>
            )}

            {userInfo?.ma_nhom_nhan_vien === "admin" && (
              <Menu.Item icon={<BookOutlined />} key={"2"}>
                <Link to={`${routes.dashboard}${routes.adminPosts}`}>
                  Bài viết
                </Link>
              </Menu.Item>
            )}

            {userInfo?.ma_nhom_nhan_vien === "admin" && (
              <Menu.Item icon={<UsergroupAddOutlined />} key={"3"}>
                <Link to={`${routes.dashboard}${routes.adminUsers}`}>
                  Người dùng
                </Link>
              </Menu.Item>
            )}

            {userInfo?.ma_nhom_nhan_vien === "admin" && (
              <Menu.Item icon={<SettingOutlined />} key={"4"}>
                <Link to={`${routes.dashboard}${routes.setting}`}>Cài đặt</Link>
              </Menu.Item>
            )}
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{ padding: 0, backgroundColor: "#fff" }}
          >
            <div className={styles.wrapper_header_right}>
              <Dropdown overlay={menu} trigger={["click"]}>
                <div
                  className={styles.wrapper_user}
                  onClick={(e) => e.preventDefault()}
                >
                  <Space>
                    <img
                      className={styles.avatar}
                      src={
                        userInfo.anh_dai_dien
                          ? `${URL_BASE}${userInfo.anh_dai_dien}`
                          : avatarDefault
                      }
                      alt="avatar"
                    />
                    {userInfo.ten_tai_khoan}
                    <DownOutlined />
                  </Space>
                </div>
              </Dropdown>
            </div>
          </Header>
          <Content style={{ margin: "15px 15px 0 15px" }}>
            <div
              className="site-layout-background"
              style={{
                padding: 15,
                minHeight: 500,
                backgroundColor: "#fff",
                borderRadius: 3,
              }}
            >
              <Outlet />
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            My App ©2022 Created by PhucVu
          </Footer>
        </Layout>
      </Layout>
      <Modal
        title="Xác nhận"
        open={isModalLogout}
        onOk={handleOkLogout}
        onCancel={handleCancelLogout}
      >
        <p>Bạn có chắc chắn muốn đăng xuất?</p>
      </Modal>
    </>
  );
}

export default DashBoard;

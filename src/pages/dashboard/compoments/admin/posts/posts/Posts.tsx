import { useEffect, useState } from "react";
import styles from "./Posts.module.scss";
import {
  Col,
  Row,
  Button,
  Space,
  Table,
  Tag,
  Modal,
  Form,
  Input,
  Select,
} from "antd";
import {
  PlusOutlined,
  EditOutlined,
  DeleteOutlined
} from "@ant-design/icons";
import type { ColumnsType } from "antd/es/table";
import Loading from "../../../../../../components/loading/Loading";
import { useAppDispatch } from "../../../../../../redux/hooks/hooks";
import { useSelector } from "react-redux";
import {
  deletePost,
  fetchPosts,
  updatePost,
  addPost
} from "../../../../../../redux/posts/action";
import {
  IBodyAddPost,
  IBodyUpdatePost,
  IDataTablePosts,
} from "../../../../../../core/models/models";
import { URL_BASE } from "../../../../../../core/constants/constans";
import moment from "moment";
import { get } from "lodash";
import { v4 as uuidv4 } from "uuid";
import ReactHtmlParser from "react-html-parser";

import thumbnailDefault from "../../../../../../assets/images/thumbnail.png";

const { TextArea } = Input;

function Posts() {
  const posts = useSelector((state: any) => state.posts);
  const [currentId, setCurrentId] = useState<number | null>(null);
  const [currentPost, setCurrentPost] = useState<any>(null);
  const [isModalOpenDeletePost, setIsModalOpenDeletePost] =
    useState<boolean>(false);
  const [isModalOpenAddPost, setIsModalOpenAddPost] = useState<boolean>(false);
  const [isModalOpenUpdatePost, setIsModalOpenUpdatePost] =
    useState<boolean>(false);

  const dispatch = useAppDispatch();

  const columns: ColumnsType<IDataTablePosts> = [
    {
      title: "Ảnh đại diện",
      dataIndex: "anh_dai_dien",
      key: "anh_dai_dien",
      render: (text) => (
        <div className={styles.wrapper_thumbnail}>
          <img
            src={text ? `${URL_BASE}${text}` : thumbnailDefault}
            alt="thumbnail"
          />
        </div>
      ),
    },
    {
      title: "Tiêu đề",
      dataIndex: "tieu_de",
      key: "tieu_de",
      render: (text) => (
        <span className={styles.p_info}>{text ? text : "--"}</span>
      ),
    },
    {
      title: "Nội dung",
      dataIndex: "noi_dung",
      key: "noi_dung",
      render: (text) => (
        <span className={styles.p_info}>
          {text ? ReactHtmlParser(text) : "--"}
        </span>
      ),
    },
    {
      title: "Mô tả",
      dataIndex: "mo_ta",
      key: "mo_ta",
      render: (text) => (
        <span className={styles.p_info}>{text ? text : "--"}</span>
      ),
    },
    {
      title: "Người tạo",
      dataIndex: "nguoi_tao",
      key: "nguoi_tao",
      render: (text) => (
        <span className={styles.p_info}>{text ? text : "--"}</span>
      ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "ngay_tao",
      key: "ngay_tao",
      render: (text) => (
        <span className={styles.p_info}>
          {text ? moment(text).format("DD/MM/YYYY | HH:mm") : "--"}
        </span>
      ),
    },
    {
      title: "Người sửa",
      dataIndex: "nguoi_sua",
      key: "nguoi_sua",
      render: (text) => (
        <span className={styles.p_info}>{text ? text : "--"}</span>
      ),
    },
    {
      title: "Ngày sửa",
      dataIndex: "ngay_sua",
      key: "ngay_sua",
      render: (text) => (
        <span className={styles.p_info}>
          {text ? moment(text).format("DD/MM/YYYY | HH:mm") : "--"}
        </span>
      ),
    },
    {
      title: "Trạng thái",
      key: "trang_thai",
      dataIndex: "trang_thai",
      render: (text) => (
        <Tag color={text === "active" ? "green" : "red"}>
          {text ? text : "--"}
        </Tag>
      ),
    },
    {
      title: "Hành động",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            onClick={() => showModalUpdatePost(record)}
            type="primary"
            size="small"
            icon={<EditOutlined />}
          >
            Sửa
          </Button>
          <Button
            onClick={() => showModalDeletePost(record?.tin_tuc_id)}
            type="primary"
            size="small"
            icon={<DeleteOutlined />}
            danger
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];

  // Handle delete post
  const showModalDeletePost = (id: number) => {
    setCurrentId(id);
    setIsModalOpenDeletePost(true);
  };

  const handleOkDeletePost = () => {
    handleDeletePost(currentId);
    setIsModalOpenDeletePost(false);
  };

  const handleCancelDeletePost = () => {
    setIsModalOpenDeletePost(false);
  };

  const handleDeletePost = (id: number | null) => {
    if (id) {
      const convertId: string = String(id);
      dispatch(deletePost(convertId));
    } else {
      return;
    }
  };

  // Handle add post
  const [formAddPost] = Form.useForm();
  const showModalAddPost = () => {
    formAddPost.resetFields();
    setIsModalOpenAddPost(true);
  };

  const handleOkAddPost = () => {
    setIsModalOpenAddPost(false);
  };

  const handleCancelAddPost = () => {
    setIsModalOpenAddPost(false);
  };

  const onFinishAddPost = (values: any) => {
    if (
      values &&
      values.tieu_de &&
      values.nguoi_nhan &&
      values.mo_ta &&
      values.noi_dung &&
      values.nhom_tin_tuc_id &&
      values.tin_noi_bat &&
      values.tin_moi &&
      values.trang_thai
    ) {
      const body: IBodyAddPost = {
        tieu_de: values.tieu_de.trim(),
        nguoi_nhan: values.nguoi_nhan.trim(),
        mo_ta: values.mo_ta.trim(),
        noi_dung: values.noi_dung.trim(),
        nhom_tin_tuc_id: String(values.nhom_tin_tuc_id),
        tin_noi_bat: values.tin_noi_bat,
        tin_moi: values.tin_moi,
        trang_thai: values.trang_thai
      };
      dispatch(addPost(body));
      setIsModalOpenAddPost(false);
    } else {
      setIsModalOpenAddPost(false);
      return;
    }
  };

  const onFinishFailedAddPost = (errorInfo: any) => {};

  // Handle update post
  const [formUpdatePost] = Form.useForm();
  const showModalUpdatePost = (data: any) => {
    setCurrentId(data.tin_tuc_id);
    setCurrentPost(data);
    formUpdatePost.setFieldsValue({
      tieu_de: data.tieu_de,
      noi_dung: data.noi_dung,
      mo_ta: data.mo_ta,
      nhom_tin_tuc_id: data.nhom_tin_tuc_id,
      tin_noi_bat: data.tin_noi_bat,
      tin_moi: data.tin_moi,
      trang_thai: data.trang_thai,
    });
    setIsModalOpenUpdatePost(true);
  };

  const handleOkUpdatePost = () => {
    formUpdatePost.resetFields();
    setIsModalOpenUpdatePost(false);
  };

  const handleCancelUpdatePost = () => {
    formUpdatePost.resetFields();
    setIsModalOpenUpdatePost(false);
  };

  const onFinishUpdatePost = (values: any) => {
    if (
      (values && values?.tieu_de?.trim() !== currentPost?.tieu_de) ||
      values?.noi_dung?.trim() !== currentPost?.noi_dung ||
      values?.mo_ta?.trim() !== currentPost?.mo_ta ||
      values?.nhom_tin_tuc_id !== currentPost?.nhom_tin_tuc_id ||
      values?.tin_noi_bat !== currentPost?.tin_noi_bat ||
      values?.tin_moi !== currentPost?.tin_moi ||
      values?.trang_thai !== currentPost?.trang_thai
    ) {
      const body: IBodyUpdatePost = {
        tieu_de: values?.tieu_de?.trim(),
        noi_dung: values?.noi_dung?.trim(),
        mo_ta: values?.mo_ta?.trim(),
        nhom_tin_tuc_id: String(values?.nhom_tin_tuc_id),
        tin_noi_bat: values?.tin_noi_bat,
        tin_moi: values?.tin_moi,
        trang_thai: values?.trang_thai,
        id: String(currentId),
      };
      dispatch(updatePost(body));
      setIsModalOpenUpdatePost(false);
      formUpdatePost.resetFields();
    } else {
      setIsModalOpenUpdatePost(false);
      formUpdatePost.resetFields();
      return;
    }
  };

  const onFinishFailedUpdatePost = (errorInfo: any) => {};

  useEffect(() => {
    dispatch(fetchPosts());
    return () => {
      setCurrentId(null);
      setCurrentPost(null);
    };
  }, []);

  return (
    <>
      {posts.list.loading && <Loading />}
      <div className={styles.posts_page}>
        <div className={styles.wrapper_posts}>
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              <div className={styles.wrapper_button}>
                <Button
                  className={styles.btn_add_posts}
                  type="primary"
                  icon={<PlusOutlined />}
                  onClick={showModalAddPost}
                >
                  Thêm bài viết
                </Button>
              </div>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              <Table
                className={styles.main_table}
                size="small"
                columns={columns}
                dataSource={get(posts, "list.result.data", []).map(
                  (item: any) => {
                    return { ...item, key: uuidv4() };
                  }
                )}
              />

              {/* Modal confirm delete post */}
              <Modal
                title="XÁC NHẬN XÓA BÀI VIẾT"
                open={isModalOpenDeletePost}
                onOk={handleOkDeletePost}
                onCancel={handleCancelDeletePost}
              >
                <p>Bạn có chắc chắn muốn xóa bài viết này?</p>
              </Modal>

              {/* Modal add post */}
              <Modal
                title="THÊM BÀI VIẾT"
                centered
                open={isModalOpenAddPost}
                onOk={handleOkAddPost}
                onCancel={handleCancelAddPost}
                footer={null}
                width={1000}
              >
                <Form
                  name="basic"
                  onFinish={onFinishAddPost}
                  onFinishFailed={onFinishFailedAddPost}
                  autoComplete="off"
                  form={formAddPost}
                  labelCol={{ span: 4 }}
                >
                  <Form.Item
                    label="Tiêu đề"
                    name="tieu_de"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập tiêu đề bài viết!",
                      },
                    ]}
                  >
                    <Input placeholder="Tiêu đề bài viết" size="large" autoFocus={true} />
                  </Form.Item>

                  <Form.Item
                    label="Người nhận"
                    name="nguoi_nhan"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập người nhận!",
                      },
                    ]}
                  >
                    <Input placeholder="Người nhận" size="large" />
                  </Form.Item>

                  <Form.Item
                    label="Nội dung"
                    name="noi_dung"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập nội dung bài viết!",
                      },
                    ]}
                  >
                    <Input.TextArea placeholder="Nội dung bài viết" rows={8} />
                  </Form.Item>

                  <Form.Item
                    label="Mô tả"
                    name="mo_ta"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập mô tả bài viết!",
                      },
                    ]}
                  >
                    <Input.TextArea placeholder="Mô tả bài viết" rows={4} />
                  </Form.Item>

                  <Form.Item
                    label="Nhóm tin tức"
                    name="nhom_tin_tuc_id"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn nhóm tin tức bài viết!",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value={0}>Nhóm 0</Select.Option>
                      <Select.Option value={1}>Nhóm 1</Select.Option>
                      <Select.Option value={2}>Nhóm 2</Select.Option>
                      <Select.Option value={3}>Nhóm 3</Select.Option>
                      <Select.Option value={4}>Nhóm 4</Select.Option>
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Tin nổi bật"
                    name="tin_noi_bat"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn tin nổi bật!",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value={0}>Tin nổi bật 0</Select.Option>
                      <Select.Option value={1}>Tin nổi bật 1</Select.Option>
                      <Select.Option value={2}>Tin nổi bật 2</Select.Option>
                      <Select.Option value={3}>Tin nổi bật 3</Select.Option>
                      <Select.Option value={4}>Tin nổi bật 4</Select.Option>
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Tin mới"
                    name="tin_moi"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn tin mới!",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value={0}>Tin mới 0</Select.Option>
                      <Select.Option value={1}>Tin mới 1</Select.Option>
                      <Select.Option value={2}>Tin mới 2</Select.Option>
                      <Select.Option value={3}>Tin mới 3</Select.Option>
                      <Select.Option value={4}>Tin mới 4</Select.Option>
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Trạng thái"
                    name="trang_thai"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn trạng thái bài viết!",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value={"active"}>Hoạt động</Select.Option>
                      <Select.Option value={"inactive"}>
                        Không hoạt động
                      </Select.Option>
                    </Select>
                  </Form.Item>

                  <Form.Item>
                    <Button type="primary" htmlType="submit" size="large" block>
                      Thêm
                    </Button>
                  </Form.Item>
                </Form>
              </Modal>

              {/* Modal update post */}
              <Modal
                title="SỬA BÀI VIẾT"
                centered
                open={isModalOpenUpdatePost}
                onOk={handleOkUpdatePost}
                onCancel={handleCancelUpdatePost}
                footer={null}
                width={1000}
              >
                <Form
                  name="basic"
                  onFinish={onFinishUpdatePost}
                  onFinishFailed={onFinishFailedUpdatePost}
                  autoComplete="off"
                  form={formUpdatePost}
                  labelCol={{ span: 4 }}
                >
                  <Form.Item
                    label="Tiêu đề"
                    name="tieu_de"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập tiêu đề bài viết!",
                      },
                    ]}
                  >
                    <Input placeholder="Tiêu đề bài viết" size="large" />
                  </Form.Item>

                  <Form.Item
                    label="Nội dung"
                    name="noi_dung"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập nội dung bài viết!",
                      },
                    ]}
                  >
                    <Input.TextArea placeholder="Nội dung bài viết" rows={8} />
                  </Form.Item>

                  <Form.Item
                    label="Mô tả"
                    name="mo_ta"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập mô tả bài viết!",
                      },
                    ]}
                  >
                    <Input.TextArea placeholder="Mô tả bài viết" rows={4} />
                  </Form.Item>

                  <Form.Item
                    label="Nhóm tin tức"
                    name="nhom_tin_tuc_id"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn nhóm tin tức bài viết!",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value={0}>Nhóm 0</Select.Option>
                      <Select.Option value={1}>Nhóm 1</Select.Option>
                      <Select.Option value={2}>Nhóm 2</Select.Option>
                      <Select.Option value={3}>Nhóm 3</Select.Option>
                      <Select.Option value={4}>Nhóm 4</Select.Option>
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Tin nổi bật"
                    name="tin_noi_bat"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn tin nổi bật!",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value={0}>Tin nổi bật 0</Select.Option>
                      <Select.Option value={1}>Tin nổi bật 1</Select.Option>
                      <Select.Option value={2}>Tin nổi bật 2</Select.Option>
                      <Select.Option value={3}>Tin nổi bật 3</Select.Option>
                      <Select.Option value={4}>Tin nổi bật 4</Select.Option>
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Tin mới"
                    name="tin_moi"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn tin mới!",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value={0}>Tin mới 0</Select.Option>
                      <Select.Option value={1}>Tin mới 1</Select.Option>
                      <Select.Option value={2}>Tin mới 2</Select.Option>
                      <Select.Option value={3}>Tin mới 3</Select.Option>
                      <Select.Option value={4}>Tin mới 4</Select.Option>
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Trạng thái"
                    name="trang_thai"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng chọn trạng thái bài viết!",
                      },
                    ]}
                  >
                    <Select>
                      <Select.Option value={"active"}>Hoạt động</Select.Option>
                      <Select.Option value={"inactive"}>
                        Không hoạt động
                      </Select.Option>
                    </Select>
                  </Form.Item>

                  <Form.Item>
                    <Button type="primary" htmlType="submit" size="large" block>
                      Sửa
                    </Button>
                  </Form.Item>
                </Form>
              </Modal>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
}

export default Posts;

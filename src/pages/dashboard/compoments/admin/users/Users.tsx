import { useEffect, useState } from "react";
import styles from "./Users.module.scss";
import {
  Col,
  Row,
  Button,
  Space,
  Table,
  Tag,
  Modal,
  Form,
  Input,
} from "antd";
import {
  PlusOutlined,
  EditOutlined,
  DeleteOutlined,
  UserOutlined,
  LockOutlined,
  MailOutlined,
} from "@ant-design/icons";
import type { ColumnsType } from "antd/es/table";
import { useSelector } from "react-redux";
import {
  fetchUser,
  deleteUser,
  addUser,
  updateUser,
} from "../../../../../redux/users/action";
import { useAppDispatch } from "../../../../../redux/hooks/hooks";
import {
  IDataTableUsers,
  IInforUserRegister,
} from "../../../../../core/models/models";
import { get } from "lodash";
import { v4 as uuidv4 } from "uuid";
import Loading from "../../../../../components/loading/Loading";
import moment from "moment";

import avatarDefault from "../../../../../assets/images/avatar.png";

function Users() {
  const users = useSelector((state: any) => state.users);
  const [currentId, setCurrentId] = useState<string>('');
  const [currentUser, setCurrentUser] = useState<any>(null);
  const [isModalOpenDeleteUser, setIsModalOpenDeleteUser] =
    useState<boolean>(false);
  const [isModalOpenAddUser, setIsModalOpenAddUser] = useState<boolean>(false);
  const [isModalOpenUpdateUser, setIsModalOpenUpdateUser] =
    useState<boolean>(false);
  const dispatch = useAppDispatch();

  const columns: ColumnsType<IDataTableUsers> = [
    {
      title: "Tên nhân viên",
      dataIndex: "ten_nhan_vien",
      key: "ten_nhan_vien",
      render: (text) => (
        <div className={styles.wrapper_info}>
          <img src={avatarDefault} alt="avatar" />
          <p className={styles.fullname}>{text ? text : "--"}</p>
        </div>
      ),
    },
    {
      title: "Tên tài khoản",
      dataIndex: "ten_tai_khoan",
      key: "ten_tai_khoan",
      render: (text) => (
        <span className={styles.p_info}>{text ? text : "--"}</span>
      ),
    },
    {
      title: "Người tạo",
      dataIndex: "nguoi_tao",
      key: "nguoi_tao",
      render: (text) => (
        <span className={styles.p_info}>{text ? text : "--"}</span>
      ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "ngay_tao",
      key: "ngay_tao",
      render: (text) => (
        <span className={styles.p_info}>
          {text ? moment(text).format("DD/MM/YYYY | HH:mm") : "--"}
        </span>
      ),
    },
    {
      title: "Người sửa",
      dataIndex: "nguoi_sua",
      key: "nguoi_sua",
      render: (text) => (
        <span className={styles.p_info}>{text ? text : "--"}</span>
      ),
    },
    {
      title: "Ngày sửa",
      dataIndex: "ngay_sua",
      key: "ngay_sua",
      render: (text) => (
        <span className={styles.p_info}>
          {text ? moment(text).format("DD/MM/YYYY | HH:mm") : "--"}
        </span>
      ),
    },
    {
      title: "Số điện thoại",
      dataIndex: "so_dien_thoai",
      key: "so_dien_thoai",
      render: (text) => (
        <span className={styles.p_info}>{text ? text : "--"}</span>
      ),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      render: (text) => (
        <span className={styles.p_info}>{text ? text : "--"}</span>
      ),
    },
    {
      title: "Trạng thái",
      key: "trang_thai",
      dataIndex: "trang_thai",
      render: (text) => (
        <Tag color={text === "active" ? "green" : "red"}>
          {text ? text : "--"}
        </Tag>
      ),
    },
    {
      title: "Hành động",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            onClick={() => showModalUpdateUser(record)}
            type="primary"
            size="small"
            icon={<EditOutlined />}
          >
            Sửa
          </Button>
          <Button
            onClick={() => showModalDeleteUser(record?.nhan_vien_id)}
            type="primary"
            size="small"
            icon={<DeleteOutlined />}
            danger
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];

  // Handle delete user
  const showModalDeleteUser = (id: string) => {
    setCurrentId(id);
    setIsModalOpenDeleteUser(true);
  };

  const handleOkDeleteUser = () => {
    handleDeleteUser(currentId);
    setIsModalOpenDeleteUser(false);
  };

  const handleCancelDeleteUser = () => {
    setIsModalOpenDeleteUser(false);
  };

  const handleDeleteUser = (id: string) => {
    dispatch(deleteUser(id));
  };

  // Handle add user
  const [formAddUser] = Form.useForm();
  const showModalAddUser = () => {
    formAddUser.resetFields();
    setIsModalOpenAddUser(true);
  };

  const handleOkAddUser = () => {
    setIsModalOpenAddUser(false);
  };

  const handleCancelAddUser = () => {
    setIsModalOpenAddUser(false);
  };

  const onFinishAddUser = (values: IInforUserRegister) => {
    if (
      values &&
      values.ten_tai_khoan &&
      values.ten_nhan_vien &&
      values.email &&
      values.mat_khau &&
      values.xac_nhan_mat_khau
    ) {
      const body = {
        ten_tai_khoan: values.ten_tai_khoan.trim(),
        ten_nhan_vien: values.ten_nhan_vien.trim(),
        email: values.email.trim(),
        mat_khau: values.mat_khau.trim(),
        xac_nhan_mat_khau: values.xac_nhan_mat_khau.trim(),
      };
      dispatch(addUser(body));
      setIsModalOpenAddUser(false);
    } else {
      setIsModalOpenAddUser(false);
      return;
    }
  };

  const onFinishFailedAddUser = (errorInfo: any) => {};

  // Handle update user
  const [formUpdateUser] = Form.useForm();
  const showModalUpdateUser = (data: any) => {
    setCurrentId(data.nhan_vien_id);
    setCurrentUser(data);
    formUpdateUser.setFieldsValue({
      ten_tai_khoan: data.ten_tai_khoan,
      ten_nhan_vien: data.ten_nhan_vien,
    });
    setIsModalOpenUpdateUser(true);
  };

  const handleOkUpdateUser = () => {
    formUpdateUser.resetFields();
    setIsModalOpenUpdateUser(false);
  };

  const handleCancelUpdateUser = () => {
    formUpdateUser.resetFields();
    setIsModalOpenUpdateUser(false);
  };

  const onFinishUpdateUser = (values: IInforUserRegister) => {
    if (
      (values &&
      values?.ten_tai_khoan?.trim() !== currentUser?.ten_tai_khoan) ||
      values?.ten_nhan_vien?.trim() !== currentUser?.ten_nhan_vien ||
      (values.mat_khau && values.xac_nhan_mat_khau && values.mat_khau.trim() && values.xac_nhan_mat_khau.trim())
    ) {
      const body: any = {
        ten_tai_khoan: values.ten_tai_khoan ? values.ten_tai_khoan.trim() : '',
        ten_nhan_vien: values.ten_nhan_vien ? values.ten_nhan_vien.trim() : '',
        id: currentId,
      };
      if (values.mat_khau) {
        body.mat_khau = values.mat_khau.trim();
      }
      dispatch(updateUser(body));
      setIsModalOpenUpdateUser(false);
      formUpdateUser.resetFields();
    } else {
      setIsModalOpenUpdateUser(false);
      formUpdateUser.resetFields();
      return;
    }
  };

  const onFinishFailedUpdateUser = (errorInfo: any) => {};

  useEffect(() => {
    dispatch(fetchUser());
    return () => {
      setCurrentId('');
      setCurrentUser(null);
    };
  }, []);

  return (
    <>
      {users.list.loading && <Loading />}
      <div className={styles.users_page}>
        <div className={styles.wrapper_users}>
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              <div className={styles.wrapper_button}>
                <Button
                  className={styles.btn_add_users}
                  type="primary"
                  icon={<PlusOutlined />}
                  onClick={showModalAddUser}
                >
                  Thêm người dùng
                </Button>
              </div>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              <Table
                className={styles.main_table}
                size="small"
                columns={columns}
                dataSource={get(users, "list.result.data", []).map(
                  (item: any) => {
                    return { ...item, key: uuidv4() };
                  }
                )}
              />

              {/* Modal confirm delete user */}
              <Modal
                title="XÁC NHẬN XÓA NGƯỜI DÙNG"
                open={isModalOpenDeleteUser}
                onOk={handleOkDeleteUser}
                onCancel={handleCancelDeleteUser}
              >
                <p>Bạn có chắc chắn muốn xóa người dùng này?</p>
              </Modal>

              {/* Modal add user */}
              <Modal
                title="THÊM NGƯỜI DÙNG"
                open={isModalOpenAddUser}
                onOk={handleOkAddUser}
                onCancel={handleCancelAddUser}
                footer={null}
              >
                <Form
                  name="basic"
                  onFinish={onFinishAddUser}
                  onFinishFailed={onFinishFailedAddUser}
                  autoComplete="off"
                  form={formAddUser}
                >
                  <Form.Item
                    name="ten_tai_khoan"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập tên đăng nhập!",
                      },
                    ]}
                  >
                    <Input
                      prefix={<UserOutlined />}
                      placeholder="Tên đăng nhập"
                      size="large"
                      autoFocus={true}
                    />
                  </Form.Item>

                  <Form.Item
                    name="ten_nhan_vien"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập tên nhân viên!",
                      },
                    ]}
                  >
                    <Input
                      prefix={<UserOutlined />}
                      placeholder="Tên nhân viên"
                      size="large"
                    />
                  </Form.Item>

                  <Form.Item
                    name="email"
                    rules={[
                      {
                        type: "email",
                        message: "Vui lòng nhập đúng định dạng email!",
                      },
                      {
                        required: true,
                        message: "Vui lòng nhập email!",
                      },
                    ]}
                  >
                    <Input
                      prefix={<MailOutlined />}
                      placeholder="Email"
                      size="large"
                    />
                  </Form.Item>

                  <Form.Item
                    name="mat_khau"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập mật khẩu!",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input.Password
                      prefix={<LockOutlined />}
                      placeholder="Mật khẩu"
                      size="large"
                    />
                  </Form.Item>

                  <Form.Item
                    name="xac_nhan_mat_khau"
                    dependencies={["mat_khau"]}
                    hasFeedback
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập mật khẩu!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("mat_khau") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error("Không trùng khớp mật khẩu!")
                          );
                        },
                      }),
                    ]}
                  >
                    <Input.Password
                      prefix={<LockOutlined />}
                      placeholder="Xác nhận mật khẩu"
                      size="large"
                    />
                  </Form.Item>

                  <Form.Item>
                    <Button type="primary" htmlType="submit" size="large" block>
                      Thêm
                    </Button>
                  </Form.Item>
                </Form>
              </Modal>

              {/* Modal update user */}
              <Modal
                title="SỬA NGƯỜI DÙNG"
                open={isModalOpenUpdateUser}
                onOk={handleOkUpdateUser}
                onCancel={handleCancelUpdateUser}
                footer={null}
              >
                <Form
                  name="basic"
                  onFinish={onFinishUpdateUser}
                  onFinishFailed={onFinishFailedUpdateUser}
                  autoComplete="off"
                  form={formUpdateUser}
                  labelCol={{ span: 9 }}
                >
                  <Form.Item
                    label="Tên tài khoản"
                    name="ten_tai_khoan"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập tên đăng nhập!",
                      },
                    ]}
                  >
                    <Input
                      prefix={<UserOutlined />}
                      placeholder="Tên đăng nhập"
                      size="large"
                    />
                  </Form.Item>

                  <Form.Item
                    label="Tên nhân viên"
                    name="ten_nhan_vien"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập tên nhân viên!",
                      },
                    ]}
                  >
                    <Input
                      prefix={<UserOutlined />}
                      placeholder="Tên nhân viên"
                      size="large"
                    />
                  </Form.Item>

                  <Form.Item
                    label="Mật khẩu mới"
                    name="mat_khau"
                    rules={[
                      {
                        required: false,
                        message: "Vui lòng nhập mật khẩu mới!",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input.Password
                      prefix={<LockOutlined />}
                      placeholder="Mật khẩu mới"
                      size="large"
                    />
                  </Form.Item>

                  <Form.Item
                    label="Xác nhận mật khẩu mới"
                    name="xac_nhan_mat_khau"
                    dependencies={["mat_khau"]}
                    hasFeedback
                    rules={[
                      {
                        required: false,
                        message: "Vui lòng nhập mật khẩu mới!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("mat_khau") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error("Không trùng khớp mật khẩu mới!")
                          );
                        },
                      }),
                    ]}
                  >
                    <Input.Password
                      prefix={<LockOutlined />}
                      placeholder="Xác nhận mật khẩu mới"
                      size="large"
                    />
                  </Form.Item>

                  <Form.Item>
                    <Button type="primary" htmlType="submit" size="large" block>
                      Sửa
                    </Button>
                  </Form.Item>
                </Form>
              </Modal>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
}

export default Users;

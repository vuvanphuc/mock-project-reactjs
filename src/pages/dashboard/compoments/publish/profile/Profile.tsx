import { useEffect, useState } from "react";
import styles from "./Profile.module.scss";
import { Col, Row, Divider, List, Button } from "antd";
import { UserOutlined, MailOutlined, PhoneOutlined } from "@ant-design/icons";
import { auth, URL_BASE } from "../../../../../core/constants/constans";
import { getObjectLocal } from "../../../../../core/utils/localStorage";
import moment from "moment";

import avatarDefault from "../../../../../assets/images/avatar.png";

function Profile() {
  const [userInfo, setUserInfo] = useState<any>(null);
  const [userInfoMore, setUserInfoMore] = useState<string[]>([]);

  useEffect(() => {
    const userInfoLocal = getObjectLocal(auth.USER_INFO);
    if (userInfoLocal) {
      setUserInfo(userInfoLocal);
      const createdDate = userInfoLocal.ngay_tao
        ? `Ngày tạo: ${moment(userInfoLocal.ngay_tao).format('DD/MM/YYYY | HH:mm')}`
        : "Ngày tạo: --";
      const userGroup = userInfoLocal.ma_nhom_nhan_vien
        ? `Mã nhóm nhân viên: ${userInfoLocal.ma_nhom_nhan_vien}`
        : "Mã nhóm nhân viên: --";
      const userStatus = userInfoLocal.trang_thai
        ? `Trạng thái: ${userInfoLocal.trang_thai}`
        : "Trạng thái: --";
      setUserInfoMore([...userInfoMore, createdDate, userGroup, userStatus]);
    }
  }, []);

  return (
    <>
      <div className={styles.profile_page}>
        <div className={styles.wrapper_profile}>
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={4} lg={4} xl={4}>
              <div className={styles.wrapper_profile_left}>
                <div className={styles.box_avatar}>
                  <img
                    className={styles.avatar}
                    src={
                      userInfo && userInfo.anh_dai_dien
                        ? `${URL_BASE}${userInfo.anh_dai_dien}`
                        : avatarDefault
                    }
                    alt="avatar"
                  />
                </div>

                <div className={styles.box_profile_info}>
                  <ul className={styles.list_info}>
                    <li className={styles.item_info}>
                      <UserOutlined />{" "}
                      {userInfo && userInfo.ten_nhan_vien
                        ? userInfo.ten_nhan_vien
                        : "Tên nhân viên"}
                    </li>
                    <li className={styles.item_info}>
                      <PhoneOutlined />{" "}
                      {userInfo && userInfo.so_dien_thoai
                        ? userInfo.so_dien_thoai
                        : "0971 888 999"}
                    </li>
                    <li className={styles.item_info}>
                      <MailOutlined />{" "}
                      {userInfo && userInfo.email
                        ? userInfo.email
                        : "demo@gmail.com"}
                    </li>
                  </ul>

                  <Button className={styles.btn_profile} type="primary" block>
                    <span className={styles.text_br}>Đổi mật khẩu</span>
                  </Button>
                  <Button className={styles.btn_profile} type="primary" block>
                    <span className={styles.text_br}>Chỉnh sửa thông tin</span>
                  </Button>
                </div>
              </div>
            </Col>

            <Col xs={24} sm={24} md={20} lg={20} xl={20}>
              <div className={styles.wrapper_profile_right}>
                <Divider orientation="left">Thông tin thêm</Divider>
                <List
                  size="small"
                  header={null}
                  footer={null}
                  bordered
                  dataSource={userInfoMore}
                  renderItem={(item) => <List.Item>{item}</List.Item>}
                />
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
}

export default Profile;

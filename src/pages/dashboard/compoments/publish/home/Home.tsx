import { useState, useEffect } from "react";
import styles from "./Home.module.scss";
import type { RadioChangeEvent } from "antd";
import { Radio, Tabs, Col, Row, Button, Card, notification } from "antd";
import {
  PlusOutlined,
  EditOutlined,
  DeleteOutlined,
  UserOutlined,
  LockOutlined,
  MailOutlined,
  ColumnWidthOutlined,
  ColumnHeightOutlined,
  ArrowRightOutlined,
  ArrowDownOutlined,
} from "@ant-design/icons";
import httpClient from "../../../../../redux/api";
import { pathApi } from "../../../../../core/constants/pathApi";
import Loading from "../../../../../components/loading/Loading";
import { renderContentNoti } from "../../../../../core/utils/utils";
import { get } from "lodash";
import { URL_BASE } from "../../../../../core/constants/constans";

import thumbnailDefault from "../../../../../assets/images/thumbnail.png";

type TabPosition = "left" | "right" | "top" | "bottom";
const { Meta } = Card;

function Home() {
  const [mode, setMode] = useState<TabPosition>("top");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [categories, setCategories] = useState<any[]>([]);
  const [posts, setPosts] = useState<any[]>([]);

  const handleModeChange = (e: RadioChangeEvent) => {
    setMode(e.target.value);
  };

  const test = (id: any) => {
    // console.log("id", id);
  };

  const handleGetPosts = async () => {
    setIsLoading(true);
    try {
      const res = await httpClient.get(pathApi.post.posts);
      // console.log("POSTS", res);
      if (res?.data?.data && res?.data?.success) {
        setPosts([...res.data.data]);
      }
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      notification.error({ ...renderContentNoti() });
    }
  };

  // const handleGetPostByCategory = async(id: string) => {
  //   setIsLoading(true);
  //   try {
  //     const res = await httpClient.get(`${pathApi.category.postsByCategory}/${id}`);
  //     console.log('POST', res);
  //     setIsLoading(false);
  //   } catch (error) {
  //     setIsLoading(false);
  //   }
  // }

  useEffect(() => {
    const handleGetListPost = async () => {
      setIsLoading(true);
      try {
        const res = await httpClient.get(pathApi.category.categories);
        if (res?.data?.data && res?.data?.success) {
          setCategories([...res.data.data]);
          // const newId = String(res.data.data[0].nhom_tin_tuc_id);
          // handleGetPostByCategory(newId)
        }
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        notification.error({ ...renderContentNoti() });
      }
    };
    handleGetListPost();
    handleGetPosts();
  }, []);

  return (
    <>
      {isLoading && <Loading />}
      <div className={styles.home_pages}>
        <Radio.Group
          onChange={handleModeChange}
          value={mode}
          style={{ marginBottom: 8 }}
        >
          <Radio.Button value="top">
            <ArrowRightOutlined /> Layout ngang
          </Radio.Button>
          <Radio.Button value="left">
            <ArrowDownOutlined /> Layout dọc
          </Radio.Button>
        </Radio.Group>
        <Tabs
          className={styles.main_tab}
          defaultActiveKey="0"
          tabPosition={mode}
          style={{ height: "100%" }}
          items={categories.map((tab: any, i: number) => {
            const id = String(i);
            return {
              label: (
                <div className={styles.tab_title} onClick={() => test(id)}>
                  {get(tab, "ten_nhom", `Tab-${id}`)}
                </div>
              ),
              key: id,
              disabled: i === 28,
              children: (
                <div className={styles.wraper_posts}>
                  <Row gutter={[16, 16]}>
                    <Col xs={24} sm={24} md={24} lg={18} xl={18}>
                      <div className={styles.wrapper_left}>
                        <Row gutter={[16, 16]}>
                          {posts.map((item: any, index: number) => (
                            <Col key={index} xs={24} sm={24} md={8} lg={6} xl={6}>
                              <Card
                                hoverable
                                style={{ width: "100%" }}
                                cover={
                                  <img
                                    alt="example"
                                    src={
                                      item.anh_dai_dien
                                        ? `${URL_BASE}${item.anh_dai_dien}`
                                        : thumbnailDefault
                                    }
                                    className={styles.thumbnail}
                                  />
                                }
                              >
                                <Meta
                                  title={get(
                                    item,
                                    "tieu_de",
                                    "Tiêu đề bài viết"
                                  )}
                                  description={get(
                                    item,
                                    "mo_ta",
                                    "Mô tả bài viết"
                                  )}
                                />
                              </Card>
                            </Col>
                          ))}
                        </Row>
                      </div>
                    </Col>

                    <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                      <div className={styles.wrapper_right}>
                        <Button
                          className={styles.btn_add_users}
                          type="primary"
                          icon={<PlusOutlined />}
                        >
                          Tag
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </div>
              ),
            };
          })}
        />
      </div>
    </>
  );
}

export default Home;

import { useState } from "react";
import styles from "./Login.module.scss";
import { Button, Form, Input, notification  } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
import routes from "../../../../core/constants/routes";
import { auth, statusNotification } from "../../../../core/constants/constans";
import { setLocal, setObjectLocal } from "../../../../core/utils/localStorage";
import { IInforUser } from "../../../../core/models/models";
import { renderContentNoti } from "../../../../core/utils/utils";
import httpClient from "../../../../redux/api";
import { pathApi } from "../../../../core/constants/pathApi";

import logo from "../../../../assets/images/logo-login.png";

function Login() {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const navigate = useNavigate();

  const onFinish = (values: IInforUser): void => {
    if (values && values.ten_tai_khoan && values.mat_khau) {
      const body = {
        ten_tai_khoan: values.ten_tai_khoan.trim(),
        mat_khau: values.mat_khau.trim(),
      }
      handleLogin(body);
    } else {
      return;
    }
  };

  const onFinishFailed = (errorInfo: any): void => {};

  const handleLogin = async (body: IInforUser) => {
    setIsLoading(true);
    try {
      const response = await httpClient.post(pathApi.auth.login, body);
      if (response?.data?.token && response?.data?.data && response?.data?.success) {
        setIsLoading(false);
        setLocal(auth.TOKEN, response.data.token);
        setObjectLocal(auth.USER_INFO, response.data.data);
        navigate(routes.dashboard);
        notification.success({...renderContentNoti(statusNotification.login.LOGIN_SUCCESS)});
      } else {
        setIsLoading(false);
        notification.error({...renderContentNoti()});
      }
    } catch (error: any) {
      setIsLoading(false);
      if (error && error.response && error.response.data && error.response.data.error && error.response.status !== 500 && error.response.status !== 401) {
        notification.error({...renderContentNoti(statusNotification.login.LOGIN_FAIL, error.response.data.error)});
      }
    }
  }

  return (
    <>
      <div className={styles.login_page}>
        <div className={styles.wrapper_login}>
          <div className={styles.wrapper_left}></div>

          <div className={styles.wrapper_right}>
            <div className={styles.wrapper_logo}>
              <img className={styles.logo_form} src={logo} alt="logo" />
            </div>
            <div className={styles.wrapper_form}>
              <h4 className={styles.title_form}>Đăng nhập</h4>

              <Form
                name="basic"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
              >
                <Form.Item
                  name="ten_tai_khoan"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng nhập tên đăng nhập của bạn!",
                    },
                  ]}
                >
                  <Input
                    prefix={<UserOutlined />}
                    placeholder="Tên đăng nhập"
                    size="large"
                    autoFocus={true}
                  />
                </Form.Item>

                <Form.Item
                  name="mat_khau"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng nhập mật khẩu của bạn!",
                    },
                  ]}
                >
                  <Input.Password
                    prefix={<LockOutlined />}
                    placeholder="Mật khẩu"
                    size="large"
                  />
                </Form.Item>

                <Form.Item>
                  <Button type="primary" htmlType="submit" size="large" loading={isLoading} block>
                    Đăng nhập
                  </Button>
                </Form.Item>
              </Form>

              <p className={styles.forgot_password}>
                <Link to={`${routes.register}`}>Đăng ký</Link>
                <span className={styles.break}>|</span>
                <Link to={`${routes.forgot}`}>Quên mật khẩu</Link>
              </p>
            </div>

            <div className={styles.wrapper_copyright}>
              <p className={styles.no_copyright}>
                Copyright 2022 © PhucVu. All rights reserved
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;

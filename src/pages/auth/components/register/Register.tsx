import { useState } from "react";
import styles from "./Register.module.scss";
import { Button, Form, Input, notification } from "antd";
import { UserOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
import { IInforUserRegister } from "../../../../core/models/models";
import httpClient from "../../../../redux/api";
import { pathApi } from "../../../../core/constants/pathApi";
import { renderContentNoti } from "../../../../core/utils/utils";
import { statusNotification } from "../../../../core/constants/constans";
import routes from "../../../../core/constants/routes";

function Register() {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const navigate = useNavigate();

  const onFinish = (values: IInforUserRegister) => {
    if (values && values.ten_tai_khoan && values.ten_nhan_vien && values.email && values.mat_khau && values.xac_nhan_mat_khau) {
      const body = {
        ten_tai_khoan: values.ten_tai_khoan.trim(),
        ten_nhan_vien: values.ten_nhan_vien.trim(),
        email: values.email.trim(),
        mat_khau: values.mat_khau.trim(),
        xac_nhan_mat_khau: values.xac_nhan_mat_khau.trim(),
      }
      handleRegister(body);
    } else {
      return;
    }
  };

  const onFinishFailed = (errorInfo: any) => {};

  const handleRegister = async (body: IInforUserRegister) => {
    setIsLoading(true);
    try {
      const response = await httpClient.post(pathApi.auth.regiter, body);
      if (response && response.data && response.data.token && response.data.data && response.data.success) {
        setIsLoading(false);
        navigate(routes.login);
        notification.success({...renderContentNoti(statusNotification.register.REGISTER_SUCCESS)});
      } else {
        setIsLoading(false);
        notification.success({...renderContentNoti()});
      }
    } catch (error: any) {
      setIsLoading(false);
      if (error && error.response && error.response.data && error.response.data.error && !error.response.data.success && error.response.status !== 500 && error.response.status !== 401) {
        notification.success({...renderContentNoti(statusNotification.register.REGISTER_FAIL, error.response.data.error)});
      }
    }
  }

  return (
    <>
      <div className={styles.register_page}>
        <div className={styles.wrapper_register}>
          <div className={styles.wrapper_form_register}>
            <h4 className={styles.title_form}>Đăng ký</h4>

            <Form
              name="basic"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                name="ten_tai_khoan"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập tên đăng nhập của bạn!",
                  },
                ]}
              >
                <Input
                  prefix={<UserOutlined />}
                  placeholder="Tên đăng nhập"
                  size="large"
                  autoFocus={true}
                />
              </Form.Item>

              <Form.Item
                name="ten_nhan_vien"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập tên nhân viên của bạn!",
                  },
                ]}
              >
                <Input
                  prefix={<UserOutlined />}
                  placeholder="Tên nhân viên"
                  size="large"
                />
              </Form.Item>

              <Form.Item
                name="email"
                rules={[
                  {
                    type: "email",
                    message: "Vui lòng nhập đúng định dạng email!",
                  },
                  {
                    required: true,
                    message: "Vui lòng nhập email của bạn!",
                  },
                ]}
              >
                <Input
                  prefix={<MailOutlined />}
                  placeholder="Email"
                  size="large"
                />
              </Form.Item>

              <Form.Item
                name="mat_khau"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập mật khẩu của bạn!",
                  },
                ]}
                hasFeedback
              >
                <Input.Password
                  prefix={<LockOutlined />}
                  placeholder="Mật khẩu"
                  size="large"
                />
              </Form.Item>

              <Form.Item
                name="xac_nhan_mat_khau"
                dependencies={["mat_khau"]}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập mật khẩu của bạn!",
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue("mat_khau") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error("Không trùng khớp mật khẩu!")
                      );
                    },
                  }),
                ]}
              >
                <Input.Password
                  prefix={<LockOutlined />}
                  placeholder="Xác nhận mật khẩu"
                  size="large"
                />
              </Form.Item>

              <Form.Item>
                <Button type="primary" htmlType="submit" size="large" loading={isLoading} block>
                  Đăng ký
                </Button>
              </Form.Item>
            </Form>

            <p className={styles.forgot_password}>
              <Link to={"/"}>Đăng nhập</Link>
              <span className={styles.break}>|</span>
              <Link to={"/forgot"}>Quên mật khẩu</Link>
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Register;

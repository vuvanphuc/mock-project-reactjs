import styles from "./Forgot.module.scss";
import { Button, Form, Input } from "antd";
import { MailOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";

function Forgot() {
  const onFinish = (values: any) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <div className={styles.forgot_page}>
        <div className={styles.wrapper_forgot}>
          <div className={styles.wrapper_form_forgot}>
            <h4 className={styles.title_form}>Quên mật khẩu</h4>

            <Form
              name="basic"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                name="email"
                rules={[
                  { type: "email", message: 'Email không đúng định dạng!'},
                  {
                    required: true,
                    message: "Vui lòng nhập email của bạn!",
                  },
                ]}
              >
                <Input
                  prefix={<MailOutlined />}
                  placeholder="Email của bạn"
                  size="large"
                  autoFocus={true}
                />
              </Form.Item>

              <Form.Item>
                <Button type="primary" htmlType="submit" size="large" block>
                  Quên mật khẩu
                </Button>
              </Form.Item>
            </Form>

            <p className={styles.forgot_password}>
              <Link to={"/"}>Đăng nhập</Link>
              <span className={styles.break}>|</span>
              <Link to={"/register"}>Đăng ký</Link>
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Forgot;

import styles from './Loading.module.scss';
import { Spin } from 'antd';

function Loading() {
  return (
    <div className={styles.loading_global}>
      <div className={styles.loading}>
        <Spin size="large"/>
      </div>
    </div>
  );
}

export default Loading;


import { Navigate, Outlet } from "react-router-dom";
import { auth } from "../../core/constants/constans";
import routes from "../../core/constants/routes";
import { getLocal } from "../../core/utils/localStorage";

function Guard() {
  const isAuth = getLocal(auth.TOKEN);

  return isAuth ? <Outlet /> : <Navigate to={routes.login} />
}

export default Guard;

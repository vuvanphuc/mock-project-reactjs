import { Button, Result } from "antd";
import { Link } from "react-router-dom";
import routes from "../../core/constants/routes";

function NotFound() {
  return (
    <>
      <Result
        status="403"
        title="403"
        subTitle="Page not found."
        extra={<Link to={routes.dashboard}><Button type="primary">Trang chủ</Button></Link>}
      />
    </>
  );
}

export default NotFound;

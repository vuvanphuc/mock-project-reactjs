import { useState } from "react";
import { Navigate, Outlet } from "react-router-dom";
import { auth } from "../../core/constants/constans";
import routes from "../../core/constants/routes";
import { getLocal, getObjectLocal } from "../../core/utils/localStorage";

function GuardAdmin() {
  const [userInfo, setUserInfo] = useState<any>(() => {
    const userInfoLocal = localStorage.getItem(auth.USER_INFO);
    if (userInfoLocal) {
      return getObjectLocal(auth.USER_INFO);
    } else {
      return {};
    }
  });
  const isAuth = getLocal(auth.TOKEN);

  return isAuth && userInfo?.ma_nhom_nhan_vien === 'admin' ? <Outlet /> : <Navigate to={routes.dashboard} />
}

export default GuardAdmin;
